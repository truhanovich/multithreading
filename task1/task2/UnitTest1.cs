﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.Threading;
using System.Runtime.Remoting.Messaging;

namespace task2
{
    [TestClass]
    public class UnitTest1
    {
        static AsyncLocal<string> _asyncLocalString = new AsyncLocal<string>();

        static ThreadLocal<string> _threadLocalString = new ThreadLocal<string>();

        [ThreadStatic]
        private static string Foo = "Value 1";

        static async Task AsyncMethodA()
        {

            CallContext.SetData("vall", "Value 1");
            _asyncLocalString.Value = "Value 1";
            _threadLocalString.Value = "Value 1";
            var t1 = AsyncMethodB("Value 1");

           // Foo = "Value 2";
           // CallContext.SetData("vall", "Value 2");
            _asyncLocalString.Value = "Value 2";
            _threadLocalString.Value = "Value 2";
            var t2 = AsyncMethodB("Value 2");

            // Await both calls
            await t1;
            await t2;
        }

        static async Task AsyncMethodB(string expectedValue)
        {
            Console.WriteLine("Entering AsyncMethodB.");
            Console.WriteLine("   Expected '{0}', AsyncLocal: '{1}', ThreadLocal: '{2}', ThreadStatic:'{3}', CallContext:'{4}'",
                              expectedValue, _asyncLocalString.Value, _threadLocalString.Value, Foo, CallContext.GetData("vall"));
            await Task.Delay(100);
            Console.WriteLine("Exiting AsyncMethodB.");
            Console.WriteLine("   Expected '{0}', got '{1}', ThreadLocal:'{2}', ThreadStatic:'{3}', CallContext:'{4}'",
                              expectedValue, _asyncLocalString.Value, _threadLocalString.Value, Foo, CallContext.GetData("vall"));
        }

        [TestMethod]
        public void TestMethod1()
        {
            AsyncMethodA().Wait();
        }
    }
}
