﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace task5
{
    public class ClassThread
    {
        private static ManualResetEvent mre;

        public static List<Thread> Thrd;

        public ClassThread(string name, int count)
        {
            Thrd = new List<Thread>(count);
            mre = new ManualResetEvent(false);
            
            for (var i = 0; i < count; i++)
            {
                var index = i;
                Thrd.Add(new Thread(() => Run(index)));
                Thrd[i].Name = $"{name}-{index++}";
            }
            mre.Set();
        }

        public void Start()
        {
            int index = Thrd.FindIndex(t => t.ThreadState==ThreadState.Unstarted);
            Thrd[index].Start();
            Thread.Sleep(50);
            mre.Reset();
        }

        void Run(int index)
        {
            Console.WriteLine("Thrd: " + Thrd[index].Name);
            mre.WaitOne();
            
            
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(Thrd[index].Name);
                Thread.Sleep(500);
            }

            Console.WriteLine(Thrd[index].Name + " end!");

            mre.Set();
        }
    }
}

