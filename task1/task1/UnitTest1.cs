﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace task1
{
    [TestClass]
    public class UnitTest1
    {
        private const uint STACK_RESERVED_SPACE = 4096 * 16;
        private const int MEGABYTE = 1024 * 1024;

        private struct MEMORY_BASIC_INFORMATION
        {
            public uint BaseAddress;
            public uint AllocationBase;
            public uint AllocationProtect;
            public uint RegionSize;
            public uint State;
            public uint Protect;
            public uint Type;
        }

        [DllImport("kernel32.dll")]
        private static extern int VirtualQuery(
        IntPtr lpAddress,
        ref MEMORY_BASIC_INFORMATION lpBuffer,
        int dwLength);

        private static double EstimatedRemainingStackSizeInMegabytes()
        {
            MEMORY_BASIC_INFORMATION stackInfo = new MEMORY_BASIC_INFORMATION();
            int size = Marshal.SizeOf<MEMORY_BASIC_INFORMATION>();
            IntPtr currentAddr = Marshal.AllocCoTaskMem(size);

            VirtualQuery(currentAddr, ref stackInfo, Marshal.SizeOf<MEMORY_BASIC_INFORMATION>());
            return (double)(currentAddr.ToInt64() - stackInfo.AllocationBase - STACK_RESERVED_SPACE) / MEGABYTE;
        }

        private static void func()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Поток 2 выводит " + i.ToString());
                Thread.Sleep(0);
            }
        }

        private void Watch(int count)
        {
            List<Thread> t = new List<Thread>();
            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                 t.Add(new Thread(func));
            }

            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds / count);
        }

        [TestMethod]
        public void TestMethod1()
        {
            Console.WriteLine("Main thread");
            Console.WriteLine("{0} mb.", EstimatedRemainingStackSizeInMegabytes());
        }

        [TestMethod]
        public void TestMethod2()
        {
            //1-2-50-100-500
            Watch(1);
            Watch(2);
            Watch(50);
            Watch(100);
            Watch(500);
            Watch(50000);
        }
    }
}
