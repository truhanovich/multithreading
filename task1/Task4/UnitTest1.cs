﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

namespace Task4
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod()
        {
            var backgroundWorker1 = new AbortableThread();

            backgroundWorker1.Start(10000);
            Thread.Sleep(100);
            backgroundWorker1.Cancel();
        }
    }

    public class AbortableThread
    {
        CancellationTokenSource cts;
        Thread th;

        public void Start(int count)
        {
            cts = new CancellationTokenSource();

            th = new Thread(() => SomeWork(count, cts.Token));
            th.IsBackground = false;
            th.Start();
        }

        int SomeWork(int count, CancellationToken cancellationToken)
        {
            int result = 0;
            while (count > result)
            {
                result++;
                Console.WriteLine("Thread2: {0}", result);
                Thread.Sleep(1);

                try
                {
                    cancellationToken.ThrowIfCancellationRequested();
                }
                catch
                {
                    th.Abort();
                }
            }
            return result;
        }

        public void Cancel()
        {
            Console.WriteLine("Cancel");
            cts.Cancel();
        }
    }
}
